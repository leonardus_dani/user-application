/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
jQuery(function (){
    var wrapper = $('#user-form');

    $('#addAlamat').on('click',function (e){
        e.preventDefault();
        var i=0;
        $('.alamat').each(function(e){
            i++;
        });
        $('#submit_user').before('<div class="form-group alamat-field"><label for="alamat${'+i+'}" class="control-label col-xs-2">Alamat'+(i+1)+'</label><div class="col-xs-5"><input class="alamat form-control" name="alamat" id="alamat'+i+'" type="text" placeholder="masukkan alamat" required=""></div><a href="#" class="btn btn-primary remove_field">-</a></div>');
        $('#lastIndex').val(i+1);
    });

    $(wrapper).on("click",".remove_field", function(e){
        e.preventDefault();
        $(this).parent('div').remove();
    });

});


