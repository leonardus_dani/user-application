package userproject

import org.springframework.dao.DataIntegrityViolationException

class IndexController {

    def index() {
        [contohMember : Member.list()]
    }

    def create(){
        redirect(controller: 'Member',action: 'create')
    }

    def edit(){
        redirect(controller: 'Member' , action: 'edit' ,params: [memberId : params.memberId])
    }

    def delete(){
        def member1 = Member.findById(params.memberId)
        try{
            member1.deleted = true
            flash.message = "Successfully deleted"
        }catch(DataIntegrityViolationException ex){
            println ex.message
        }
        redirect(action: 'index')
    }
}
