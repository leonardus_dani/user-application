package userproject

import org.springframework.web.servlet.ModelAndView

class MemberController {

    static defaultAction = "create"

    def create(){

    }
    def save(){
        if(params.id){
            def member = Member.findById(params.id)
            member.properties = params
            if (!member.save()) {
                member.errors.each {
                    println it
                }
            }
        }else{
            def member = new Member(params)
            member.deleted = false
            if (!member.save()) {
                member.errors.each {
                    println it
                }
            }
        }
        redirect(controller: 'Index', action: 'index')
    }

    def edit(){
        def member = Member.findById(params.memberId)
        return new ModelAndView("/member/create",[editMember : member])
    }
}
