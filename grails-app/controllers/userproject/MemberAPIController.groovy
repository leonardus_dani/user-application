package userproject

import grails.converters.JSON


class MemberAPIController {

    def show(){
        if (params.id && Member.exists(params.id)) {
            def p = Member.findById(params.id)
            render p as JSON
        }
        else {
            def all = Member.list()
            render all as JSON
        }
    }

    def update(){
        def member = Member.findById(params.id)
        member.properties = params
        member.save()
    }

    def delete(){
        def member = Member.findById(params.id)
        member.delete(flush: true)
    }

    def save(){
        def member1 = new Member(params.member)
        member1.save(flush: true)
    }
}
