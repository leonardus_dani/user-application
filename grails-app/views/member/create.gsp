<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'create.js')}"></script>
    <title>User Application</title>
</head>

<body>
<g:message code="${flash.message}"></g:message>
<g:if test="${editMember}">
    <g:form class="form-horizontal" name="user-form" controller="member" action="save">
        <br/>
        <g:hiddenField name="id" value="${editMember.id}"></g:hiddenField>
        <div class="form-group">
            <label for="nama" class="control-label col-xs-2">Nama</label>

            <div class="col-xs-5">
                <g:textField class="form-control" value="${editMember.nama}" name="nama" placeholder="masukkan nama"
                             maxlength="50" required=""></g:textField>
            </div>
        </div>

        <div class="form-group">
            <label for="nomorHP" class="control-label col-xs-2">Nomor HP</label>

            <div class="col-xs-5">
                <g:textField name="nomorHP" value="${editMember.nomorHP}" class="form-control" pattern="[0-9].{9,}"
                             placeholder="masukkan nomor hp" required=""></g:textField>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="control-label col-xs-2">Email</label>

            <div class="col-xs-5">
                <g:field class="form-control" value="${editMember.email}" type="email" name="email"
                         placeholder="masukkan email" required=""></g:field>
            </div>
        </div>
        <g:each in="${editMember.alamat}" var="alamat" status="i">
            <div class="form-group alamat-field">
                <label for="alamat" class="control-label col-xs-2">Alamat ${(i + 1)}</label>

                <div class="col-xs-5">
                    <g:textField class="form-control alamat" value="${alamat}" name="alamat"
                                 placeholder="masukkan alamat" required=""></g:textField>
                </div>
                <g:if test="${i == 0}">
                    <a id="addAlamat" class="btn btn-primary">+</a><br id="lastAlamat"/>
                </g:if>
                <g:else>
                    <a href="#" class="btn btn-primary remove_field">-</a>
                </g:else>
            </div>
        </g:each>

        <p id="submit_user" class="col-sm-4" align="right">
            <a class="btn btn-danger" href="${createLink(uri: '/')}">Back</a>
            <g:actionSubmit class="btn btn-success" value="Save"></g:actionSubmit>
        </p>
    </g:form>
</g:if>
<g:if test="${!editMember}">
    <g:form class="form-horizontal" name="user-form" controller="member" action="save">
        <br/>
        <g:hiddenField name="id"></g:hiddenField>
        <div class="form-group">
            <label for="nama" class="control-label col-xs-2">Nama</label>

            <div class="col-xs-5">
                <g:textField class="form-control" name="nama" placeholder="masukkan nama" maxlength="50"
                             required=""></g:textField>
            </div>
        </div>

        <div class="form-group">
            <label for="nomorHP" class="control-label col-xs-2">Nomor HP</label>

            <div class="col-xs-5">
                <g:textField name="nomorHP" class="form-control" pattern="[0-9].{9,}" placeholder="masukkan nomor hp"
                             required=""></g:textField>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="control-label col-xs-2">Email</label>

            <div class="col-xs-5">
                <g:field class="form-control" type="email" name="email" placeholder="masukkan email"
                         required=""></g:field>
            </div>
        </div>

        <div class="form-group alamat-field">
            <label for="alamat" class="control-label col-xs-2">Alamat 1</label>

            <div class="col-xs-5">
                <g:textField class="form-control alamat" name="alamat" placeholder="masukkan alamat"
                             required=""></g:textField>
            </div>
            <a id="addAlamat" class="btn btn-primary">+</a><br id="lastAlamat"/>

        </div>

        <p id="submit_user" class="col-sm-4" align="right">
            <a class="btn btn-danger" href="${createLink(uri: '/')}">Back</a>
            <g:actionSubmit class="btn btn-success" value="Save"></g:actionSubmit>
        </p>
    </g:form>
</g:if>
</body>
</html>