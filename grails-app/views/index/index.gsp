<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'index.js')}"></script>
    <title>User Application</title>
    <style type="text/css">
        #table_user_filter{
            display:none;
        }
    </style>
</head>
<body>
<g:if test="${flash.message}">
    <div id="flashMessage" class="alert alert-success" style="display: block">${flash.message}</div>
</g:if>
<g:if test="${contohMember}">
<p class="form-inline col-sm-12" align="right">NAMA <input class="form-control" id="search_input" type="text"></p>
<table class="table table-striped " id="table_user">
    <thead>
    <tr>
        <td>Nama</td>
        <td>Email</td>
        <td>Delete</td>
        <td>Edit</td>
    </tr>
    </thead>
    <tbody>
    <g:each in="${contohMember}" var="member">
    <tr>
        <td>${member.nama}</td>
        <td>${member.email}</td>
        <td><g:link action="delete" params="${[memberId : member.id]}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">Delete</g:link></td>
        <td><g:link action="edit" params="${[memberId: member.id]}">Edit</g:link> </td>
    </tr>
    </g:each>
    </tbody>
</table>
</g:if>
<g:else>
<H1>There's Currently No Data Exists Please Create By Clicking The Add Button Bellow</H1>
</g:else>
<br/>
<p class="col-sm-12" align="right">
<g:link action="create"><button class="btn btn-primary">Add</button></g:link>
</p>
</body>
</html>