<!DOCTYPE html>
	<head>
		<title><g:layoutTitle default="Grails"/></title>
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.dataTables.min.css')}" type="text/css">
		<script type="text/javascript" src="${resource(dir: 'js', file: 'jquery-1.10.1.min.js')}"></script>
		<script type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>
		<script type="text/javascript" src="${resource(dir: 'js', file: 'jquery.dataTables.min.js')}"></script>
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
		<div>
		<nav class="nav navbar-fixed-top navbar-default navbar-inverse" role="navigation">
				<div class="navbar-header">
					<a class="navbar-brand" href="#"><b>USER APPLICATION</b></a>
				</div>
		</nav>
		</div>
		<br><br><br>
		<g:layoutBody/>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
