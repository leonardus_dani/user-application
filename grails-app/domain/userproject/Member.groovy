package userproject


class Member {

    String nama
    String nomorHP
    String email
    Boolean deleted
    static hasMany = [alamat : String]

    static hibernateFilters = {
        enabledFilter(condition:'deleted=0')
    }

    static constraints = {
        nama blank: false, maxSize: 50
        nomorHP blank: false, minSize: 10
        email blank: false, email: true
        alamat blank: false
    }
}
