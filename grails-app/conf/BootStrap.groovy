import userproject.Member

class BootStrap {

    def init = { servletContext ->
        def member1 = new Member(nama: 'Budi Susanto',
            nomorHP: '08562348765',
            deleted: false,
            email: 'budi@gmail.com')
        member1.save()
        member1.addToAlamat('demangan')
        member1.addToAlamat('wirobrajan')
        member1.save()
    }
    def destroy = {
    }
}
