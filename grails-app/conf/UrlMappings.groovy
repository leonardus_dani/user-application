class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
		"/api/member/$id?"(controller: "MemberAPI", parseRequest: true){
			action = [GET: "show", PUT: "update", DELETE: "delete", POST: "save"]
		}
		"/"(controller: 'Index',action: 'index')
		"500"(view:'/error')
	}
}
